﻿// -----------------------------------------------------------------------------
//  Copyright (C) 2016-2019 GoodSeat
//  Distributed under the MIT License
//  See https://sites.google.com/site/eatbaconandham/clapte/license 
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;

namespace GoodSeat.Clapte.Views
{
    /// <summary>
    /// クリップボードを監視するクラスです。
    /// 参考：http://anis774.net/codevault.html
    /// </summary>
    public sealed class ClipBoardWatcher : IDisposable
    {
        ClipBoardWatcherForm _form;

        /// <summary>
        /// クリップボードに内容に変更があると発生します。
        /// </summary>
        public event EventHandler DrawClipBoard;

        /// <summary>
        /// ClipBoardWatcherクラスを初期化してクリップボードビューアチェインに登録します。
        /// </summary>
        public ClipBoardWatcher()
        {
            _form = new ClipBoardWatcherForm();
            _form.StartWatch(raiseDrawClipBoard);
            Enable = true;
        }

        /// <summary>
        /// クリップボードの監視有無を設定もしくは取得します。
        /// </summary>
        public bool Enable
        {
            get { return _form.Enable; }
            set { _form.Enable = value; }
        }

        private void raiseDrawClipBoard()
        {
#if DEBUG
            Console.WriteLine("クリップボードに変化がありました。");
#endif
            if (DrawClipBoard != null) DrawClipBoard(this, EventArgs.Empty);
        }

        /// <summary>
        /// ClipBoardWatcherクラスをクリップボードビューアチェインから削除します。
        /// </summary>
        public void Dispose()
        {
            _form.Dispose();
        }

        private class ClipBoardWatcherForm : Form
        {
            DateTime _lastProcDate;

            [DllImport("user32.dll")]
            private static extern IntPtr SetClipboardViewer(IntPtr hwnd);
            [DllImport("user32.dll")]
            private static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);
            [DllImport("user32.dll")]
            private static extern bool ChangeClipboardChain(IntPtr hwnd, IntPtr hWndNext);

            const int WM_DRAWCLIPBOARD = 0x0308;
            const int WM_CHANGECBCHAIN = 0x030D;

            IntPtr _nextHandle;
            ThreadStart _proc;

            public bool Enable { get; set; }

            public void StartWatch(ThreadStart proc)
            {
                _proc = proc;
                _nextHandle = SetClipboardViewer(Handle);
                WindowState = FormWindowState.Minimized;

                _lastProcDate = DateTime.Now;
            }

            protected override void WndProc(ref Message m)
            {
                if (Enable && m.Msg == WM_DRAWCLIPBOARD)
                {
#if DEBUG
                    Console.WriteLine(this.Handle.ToString() + ", " + _nextHandle.ToString());
#endif
                    SendMessage(_nextHandle, m.Msg, m.WParam, m.LParam);

                    if (DateTime.Now - _lastProcDate > TimeSpan.FromMilliseconds(100))
                    {
                        _proc();
                        _lastProcDate = DateTime.Now;
                    }
                }
                else if (Enable && m.Msg == WM_CHANGECBCHAIN)
                {
                    if (m.WParam == _nextHandle)
                    {
                        _nextHandle = m.LParam;
                    }
                    else
                    {
                        SendMessage(_nextHandle, m.Msg, m.WParam, m.LParam);
                    }
                }
                base.WndProc(ref m);
            }

            protected override void Dispose(bool disposing)
            {
                try
                {
                    ChangeClipboardChain(this.Handle, _nextHandle);
                }
                catch { }
                base.Dispose(disposing);
            }
        }
    }
}
