﻿namespace GoodSeat.Clapte.Views.Forms
{
    partial class FormOfClaptePad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Sgry.Azuki.FontInfo fontInfo1 = new Sgry.Azuki.FontInfo();
            Sgry.Azuki.FontInfo fontInfo2 = new Sgry.Azuki.FontInfo();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOfClaptePad));
            this._inputTextBox = new Sgry.Azuki.WinForms.AzukiControl();
            this._contextMenuEdit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._menuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this._menuRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._menuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._menuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._menuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._menuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this._menuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this._menuFindAndReplace = new System.Windows.Forms.ToolStripMenuItem();
            this._menuVisibleDeformHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._menuJumpDefine = new System.Windows.Forms.ToolStripMenuItem();
            this._menuAddUserDefine = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this._menuCommentOut = new System.Windows.Forms.ToolStripMenuItem();
            this._menuUnCommentOut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this._menuConvertUnit = new System.Windows.Forms.ToolStripMenuItem();
            this._menuSubstitute = new System.Windows.Forms.ToolStripMenuItem();
            this._menuDefineAsConstantOfFunction = new System.Windows.Forms.ToolStripMenuItem();
            this._menuDeformAboutSelected = new System.Windows.Forms.ToolStripMenuItem();
            this._menuSolveSimultaneousEquation = new System.Windows.Forms.ToolStripMenuItem();
            this._menuInsertSigma = new System.Windows.Forms.ToolStripMenuItem();
            this._menuInsertPi = new System.Windows.Forms.ToolStripMenuItem();
            this._menuDeformFormula = new System.Windows.Forms.ToolStripMenuItem();
            this._menuExpand = new System.Windows.Forms.ToolStripMenuItem();
            this._menuTidyUp = new System.Windows.Forms.ToolStripMenuItem();
            this._menuSimplify = new System.Windows.Forms.ToolStripMenuItem();
            this._menuFactorize = new System.Windows.Forms.ToolStripMenuItem();
            this._menuCollectAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this._menuStartHotLoading = new System.Windows.Forms.ToolStripMenuItem();
            this._menuInsertHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._splitContainer = new System.Windows.Forms.SplitContainer();
            this._panelHotLoading = new System.Windows.Forms.Panel();
            this._btnSaveSync = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnStopHotLoading = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._textBoxHotReloadingPath = new System.Windows.Forms.TextBox();
            this._picHotReloading = new System.Windows.Forms.PictureBox();
            this._panelFind = new System.Windows.Forms.Panel();
            this._btnToggleFindPanelPosition = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this._btnReplaceAll = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnHideFindPanel = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnToggleFindUseRegex = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnToggleFindMatchCase = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._labelReplace = new System.Windows.Forms.Label();
            this._labelFind = new System.Windows.Forms.Label();
            this._btnReplaceAndPrev = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnReplaceAndNext = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnFindPrev = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnFindNext = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this._textBoxReplace = new System.Windows.Forms.TextBox();
            this._textBoxFind = new System.Windows.Forms.TextBox();
            this._btnAllDelete = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._panelHotSaving = new System.Windows.Forms.Panel();
            this._btnStopHotSaving = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._textBoxHotSavingPath = new System.Windows.Forms.TextBox();
            this._picHotSaving = new System.Windows.Forms.PictureBox();
            this._resultTextBox = new Sgry.Azuki.WinForms.AzukiControl();
            this._contextMenuResult = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._menuCopyResult = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this._menuSelectAllResult = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this._menuFindAndReplaceResult = new System.Windows.Forms.ToolStripMenuItem();
            this._menuJumpDefineResult = new System.Windows.Forms.ToolStripMenuItem();
            this._menuAddUserDefineResult = new System.Windows.Forms.ToolStripMenuItem();
            this._menuVisibleDeformHistoryResult = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this._menuToggleHotSaving = new System.Windows.Forms.ToolStripMenuItem();
            this._treeViewHistory = new System.Windows.Forms.TreeView();
            this._contextMenuHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._menuHideDeformHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this._menuExpandHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._menuFoldHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._picStatus = new System.Windows.Forms.PictureBox();
            this._btnAbort = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnLoad = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnSave = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._timerDelay = new System.Windows.Forms.Timer(this.components);
            this._openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this._btnSetting = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnMinimize = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._splitContainerAll = new System.Windows.Forms.SplitContainer();
            this._contextMenuHistoryNode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._menuCopyFormulaInHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this._menuHideDeformHistoryNode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this._menuExpandHistoryThisFormula = new System.Windows.Forms.ToolStripMenuItem();
            this._menuFoldHistoryThisFormula = new System.Windows.Forms.ToolStripMenuItem();
            this._menuExpandHistoryNode = new System.Windows.Forms.ToolStripMenuItem();
            this._menuFoldHistoryNode = new System.Windows.Forms.ToolStripMenuItem();
            this._btnHelp = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._btnUpdate = new GoodSeat.Clapte.Views.Components.ImageButton(this.components);
            this._toolTipFind = new System.Windows.Forms.ToolTip(this.components);
            this._expandMenuConvertUnit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._txtBoxTargetUnit = new System.Windows.Forms.ToolStripTextBox();
            this._toolTipExpand = new System.Windows.Forms.ToolTip(this.components);
            this._expandMenuExtractDefine = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._txtBoxDefineConstantOfFunctionName = new System.Windows.Forms.ToolStripTextBox();
            this._expandMenuSubstitute = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._expandMenuCollectAbout = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._txtBoxCollectAbout = new System.Windows.Forms.ToolStripTextBox();
            this._fileSystemWatcherHotLoading = new System.IO.FileSystemWatcher();
            this._contextMenuEditOnHotLoading = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._menuCopyOnHotLoading = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this._menuSelectAllOnHotLoading = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this._menuFindOnHotLoading = new System.Windows.Forms.ToolStripMenuItem();
            this._menuVisibleDeformHistoryOnHotLoading = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this._menuStopHotLoading = new System.Windows.Forms.ToolStripMenuItem();
            this._contextMenuEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._splitContainer)).BeginInit();
            this._splitContainer.Panel1.SuspendLayout();
            this._splitContainer.Panel2.SuspendLayout();
            this._splitContainer.SuspendLayout();
            this._panelHotLoading.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._btnSaveSync)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnStopHotLoading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._picHotReloading)).BeginInit();
            this._panelFind.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._btnToggleFindPanelPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnReplaceAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnHideFindPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnToggleFindUseRegex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnToggleFindMatchCase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnReplaceAndPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnReplaceAndNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnFindPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnFindNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnAllDelete)).BeginInit();
            this._panelHotSaving.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._btnStopHotSaving)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._picHotSaving)).BeginInit();
            this._contextMenuResult.SuspendLayout();
            this._contextMenuHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._picStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnAbort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._splitContainerAll)).BeginInit();
            this._splitContainerAll.Panel1.SuspendLayout();
            this._splitContainerAll.Panel2.SuspendLayout();
            this._splitContainerAll.SuspendLayout();
            this._contextMenuHistoryNode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._btnHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnUpdate)).BeginInit();
            this._expandMenuConvertUnit.SuspendLayout();
            this._expandMenuExtractDefine.SuspendLayout();
            this._expandMenuCollectAbout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._fileSystemWatcherHotLoading)).BeginInit();
            this._contextMenuEditOnHotLoading.SuspendLayout();
            this.SuspendLayout();
            // 
            // _inputTextBox
            // 
            this._inputTextBox.BackColor = System.Drawing.Color.White;
            this._inputTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._inputTextBox.ContextMenuStrip = this._contextMenuEdit;
            this._inputTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._inputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inputTextBox.DrawingOption = Sgry.Azuki.DrawingOption.HighlightsMatchedBracket;
            this._inputTextBox.DrawsEolCode = false;
            this._inputTextBox.DrawsFullWidthSpace = false;
            this._inputTextBox.DrawsTab = false;
            this._inputTextBox.FirstVisibleLine = 0;
            this._inputTextBox.Font = new System.Drawing.Font("HGｺﾞｼｯｸM", 9F);
            fontInfo1.Name = "HGｺﾞｼｯｸM";
            fontInfo1.Size = 9;
            fontInfo1.Style = System.Drawing.FontStyle.Regular;
            this._inputTextBox.FontInfo = fontInfo1;
            this._inputTextBox.ForeColor = System.Drawing.Color.Black;
            this._inputTextBox.HighlightsCurrentLine = false;
            this._inputTextBox.Location = new System.Drawing.Point(0, 0);
            this._inputTextBox.Name = "_inputTextBox";
            this._inputTextBox.ScrollPos = new System.Drawing.Point(0, 0);
            this._inputTextBox.ScrollsBeyondLastLine = false;
            this._inputTextBox.ShowsDirtBar = false;
            this._inputTextBox.ShowsHScrollBar = false;
            this._inputTextBox.ShowsLineNumber = false;
            this._inputTextBox.ShowsVScrollBar = false;
            this._inputTextBox.Size = new System.Drawing.Size(432, 440);
            this._inputTextBox.TabIndex = 16;
            this._inputTextBox.ViewWidth = 4097;
            this._inputTextBox.CaretMoved += new System.EventHandler(this._inputTextBox_CaretMoved);
            this._inputTextBox.VScroll += new System.EventHandler(this._inputTextBox_VScroll);
            this._inputTextBox.FontChanged += new System.EventHandler(this._inputTextBox_FontChanged);
            this._inputTextBox.TextChanged += new System.EventHandler(this._inputTextBox_TextChanged);
            this._inputTextBox.Enter += new System.EventHandler(this._inputTextBox_Enter);
            this._inputTextBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this._inputTextBox_MouseMove);
            // 
            // _contextMenuEdit
            // 
            this._contextMenuEdit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._menuUndo,
            this._menuRedo,
            this.toolStripSeparator1,
            this._menuCut,
            this._menuCopy,
            this._menuPaste,
            this._menuDelete,
            this.toolStripSeparator2,
            this._menuSelectAll,
            this.toolStripSeparator3,
            this._menuFindAndReplace,
            this._menuVisibleDeformHistory,
            this._menuJumpDefine,
            this._menuAddUserDefine,
            this.toolStripSeparator4,
            this._menuCommentOut,
            this._menuUnCommentOut,
            this.toolStripSeparator7,
            this._menuConvertUnit,
            this._menuSubstitute,
            this._menuDefineAsConstantOfFunction,
            this._menuDeformAboutSelected,
            this._menuSolveSimultaneousEquation,
            this._menuInsertSigma,
            this._menuInsertPi,
            this._menuDeformFormula,
            this.toolStripSeparator8,
            this._menuStartHotLoading,
            this._menuInsertHelp});
            this._contextMenuEdit.Name = "_contextMenuEdit";
            this._contextMenuEdit.Size = new System.Drawing.Size(288, 546);
            this._contextMenuEdit.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this._contextMenuEdit_Closing);
            this._contextMenuEdit.Opening += new System.ComponentModel.CancelEventHandler(this._contextMenuEdit_Opening);
            // 
            // _menuUndo
            // 
            this._menuUndo.Name = "_menuUndo";
            this._menuUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this._menuUndo.Size = new System.Drawing.Size(287, 22);
            this._menuUndo.Text = "元に戻す(&U)";
            this._menuUndo.Click += new System.EventHandler(this._menuUndo_Click);
            // 
            // _menuRedo
            // 
            this._menuRedo.Name = "_menuRedo";
            this._menuRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this._menuRedo.Size = new System.Drawing.Size(287, 22);
            this._menuRedo.Text = "やり直す(&R)";
            this._menuRedo.Click += new System.EventHandler(this._menuRedo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(284, 6);
            // 
            // _menuCut
            // 
            this._menuCut.Name = "_menuCut";
            this._menuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._menuCut.Size = new System.Drawing.Size(287, 22);
            this._menuCut.Text = "切り取り(&T)";
            this._menuCut.Click += new System.EventHandler(this._menuCut_Click);
            // 
            // _menuCopy
            // 
            this._menuCopy.Name = "_menuCopy";
            this._menuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._menuCopy.Size = new System.Drawing.Size(287, 22);
            this._menuCopy.Text = "コピー(&C)";
            this._menuCopy.Click += new System.EventHandler(this._menuCopy_Click);
            // 
            // _menuPaste
            // 
            this._menuPaste.Name = "_menuPaste";
            this._menuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._menuPaste.Size = new System.Drawing.Size(287, 22);
            this._menuPaste.Text = "貼り付け(&P)";
            this._menuPaste.Click += new System.EventHandler(this._menuPaste_Click);
            // 
            // _menuDelete
            // 
            this._menuDelete.Name = "_menuDelete";
            this._menuDelete.Size = new System.Drawing.Size(287, 22);
            this._menuDelete.Text = "削除(&D)";
            this._menuDelete.Click += new System.EventHandler(this._menuDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(284, 6);
            // 
            // _menuSelectAll
            // 
            this._menuSelectAll.Name = "_menuSelectAll";
            this._menuSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this._menuSelectAll.Size = new System.Drawing.Size(287, 22);
            this._menuSelectAll.Text = "すべて選択(&A)";
            this._menuSelectAll.Click += new System.EventHandler(this._menuSelectAll_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(284, 6);
            // 
            // _menuFindAndReplace
            // 
            this._menuFindAndReplace.Name = "_menuFindAndReplace";
            this._menuFindAndReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._menuFindAndReplace.Size = new System.Drawing.Size(287, 22);
            this._menuFindAndReplace.Text = "検索と置換(&F)";
            this._menuFindAndReplace.ToolTipText = "検索/置換パネルを表示します。";
            this._menuFindAndReplace.Click += new System.EventHandler(this._menuFindAndReplace_Click);
            // 
            // _menuVisibleDeformHistory
            // 
            this._menuVisibleDeformHistory.Name = "_menuVisibleDeformHistory";
            this._menuVisibleDeformHistory.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this._menuVisibleDeformHistory.Size = new System.Drawing.Size(287, 22);
            this._menuVisibleDeformHistory.Text = "計算過程の表示(&D)";
            this._menuVisibleDeformHistory.ToolTipText = "計算過程の表示/非表示を切り替えます。";
            this._menuVisibleDeformHistory.Click += new System.EventHandler(this._menuVisibleDeformHistory_Click);
            // 
            // _menuJumpDefine
            // 
            this._menuJumpDefine.Name = "_menuJumpDefine";
            this._menuJumpDefine.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this._menuJumpDefine.Size = new System.Drawing.Size(287, 22);
            this._menuJumpDefine.Text = "定義を参照(&J)";
            this._menuJumpDefine.ToolTipText = "カーソル位置にある定数/関数/単位の定義位置にジャンプします。";
            this._menuJumpDefine.Click += new System.EventHandler(this._menuJumpDefine_Click);
            // 
            // _menuAddUserDefine
            // 
            this._menuAddUserDefine.Name = "_menuAddUserDefine";
            this._menuAddUserDefine.Size = new System.Drawing.Size(287, 22);
            this._menuAddUserDefine.Text = "ユーザー定義に登録(&G)";
            this._menuAddUserDefine.Visible = false;
            this._menuAddUserDefine.Click += new System.EventHandler(this._menuAddUserDefine_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(284, 6);
            // 
            // _menuCommentOut
            // 
            this._menuCommentOut.Name = "_menuCommentOut";
            this._menuCommentOut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this._menuCommentOut.Size = new System.Drawing.Size(287, 22);
            this._menuCommentOut.Text = "コメントアウト(&K)";
            this._menuCommentOut.ToolTipText = "選択行の先頭に\'#\'を追加し、コメントアウトします。";
            this._menuCommentOut.Click += new System.EventHandler(this._menuCommentOut_Click);
            // 
            // _menuUnCommentOut
            // 
            this._menuUnCommentOut.Name = "_menuUnCommentOut";
            this._menuUnCommentOut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this._menuUnCommentOut.Size = new System.Drawing.Size(287, 22);
            this._menuUnCommentOut.Text = "コメントアウト解除(&L)";
            this._menuUnCommentOut.ToolTipText = "選択行の先頭にある\'#\'を削除し、コメントアウトを解除します。";
            this._menuUnCommentOut.Click += new System.EventHandler(this._menuUnCommentOut_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(284, 6);
            // 
            // _menuConvertUnit
            // 
            this._menuConvertUnit.Name = "_menuConvertUnit";
            this._menuConvertUnit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this._menuConvertUnit.Size = new System.Drawing.Size(287, 22);
            this._menuConvertUnit.Text = "単位換算(&I)";
            this._menuConvertUnit.ToolTipText = "計算結果を単位換算します。";
            this._menuConvertUnit.Click += new System.EventHandler(this._menuConvertUnit_Click);
            // 
            // _menuSubstitute
            // 
            this._menuSubstitute.Name = "_menuSubstitute";
            this._menuSubstitute.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this._menuSubstitute.Size = new System.Drawing.Size(287, 22);
            this._menuSubstitute.Text = "変数の代入(&R)";
            this._menuSubstitute.ToolTipText = "指定変数に任意値を代入し、その結果を次の行に挿入します。";
            this._menuSubstitute.Click += new System.EventHandler(this._menuSubstitute_Click);
            // 
            // _menuDefineAsConstantOfFunction
            // 
            this._menuDefineAsConstantOfFunction.Name = "_menuDefineAsConstantOfFunction";
            this._menuDefineAsConstantOfFunction.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this._menuDefineAsConstantOfFunction.Size = new System.Drawing.Size(287, 22);
            this._menuDefineAsConstantOfFunction.Text = "選択部分を定数/関数として抽出(&S)";
            this._menuDefineAsConstantOfFunction.ToolTipText = "選択部分を定数/関数で置き換え、その定義を前の行に挿入します。";
            this._menuDefineAsConstantOfFunction.Click += new System.EventHandler(this._menuDefineAsConstantOfFunction_Click);
            // 
            // _menuDeformAboutSelected
            // 
            this._menuDeformAboutSelected.Name = "_menuDeformAboutSelected";
            this._menuDeformAboutSelected.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this._menuDeformAboutSelected.Size = new System.Drawing.Size(287, 22);
            this._menuDeformAboutSelected.Text = "選択部分に関する式に変形(&O)";
            this._menuDeformAboutSelected.ToolTipText = "選択部分に関する式に変形します。";
            this._menuDeformAboutSelected.Click += new System.EventHandler(this._menuDeformAboutSelected_Click);
            // 
            // _menuSolveSimultaneousEquation
            // 
            this._menuSolveSimultaneousEquation.Name = "_menuSolveSimultaneousEquation";
            this._menuSolveSimultaneousEquation.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this._menuSolveSimultaneousEquation.Size = new System.Drawing.Size(287, 22);
            this._menuSolveSimultaneousEquation.Text = "連立方程式を解く(&S)";
            this._menuSolveSimultaneousEquation.ToolTipText = "選択行を連立方程式として求解します。";
            this._menuSolveSimultaneousEquation.Click += new System.EventHandler(this._menuSolveSimultaneousEquation_Click);
            // 
            // _menuInsertSigma
            // 
            this._menuInsertSigma.Name = "_menuInsertSigma";
            this._menuInsertSigma.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._menuInsertSigma.Size = new System.Drawing.Size(287, 22);
            this._menuInsertSigma.Text = "選択行の総和を挿入(&S)";
            this._menuInsertSigma.ToolTipText = "選択行の総和を求める式を次の行に挿入します。";
            this._menuInsertSigma.Click += new System.EventHandler(this._menuInsertSigma_Click);
            // 
            // _menuInsertPi
            // 
            this._menuInsertPi.Name = "_menuInsertPi";
            this._menuInsertPi.Size = new System.Drawing.Size(287, 22);
            this._menuInsertPi.Text = "選択行の総積を挿入(&P)";
            this._menuInsertPi.Visible = false;
            this._menuInsertPi.Click += new System.EventHandler(this._menuInsertPi_Click);
            // 
            // _menuDeformFormula
            // 
            this._menuDeformFormula.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._menuExpand,
            this._menuTidyUp,
            this._menuSimplify,
            this._menuFactorize,
            this._menuCollectAbout});
            this._menuDeformFormula.Name = "_menuDeformFormula";
            this._menuDeformFormula.Size = new System.Drawing.Size(287, 22);
            this._menuDeformFormula.Text = "数式の変形(&F)";
            this._menuDeformFormula.ToolTipText = "数式を変形し、その結果を次の行に挿入します。";
            // 
            // _menuExpand
            // 
            this._menuExpand.Name = "_menuExpand";
            this._menuExpand.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D1)));
            this._menuExpand.Size = new System.Drawing.Size(236, 22);
            this._menuExpand.Text = "展開(&E)";
            this._menuExpand.ToolTipText = "数式を展開します。\r\n(例) (x * x + 1)(x + x + 1) → 1*1 + 1*x + 1*x + 1*x*x + x*x*x + x*x*x";
            this._menuExpand.Click += new System.EventHandler(this._menuExpand_Click);
            // 
            // _menuTidyUp
            // 
            this._menuTidyUp.Name = "_menuTidyUp";
            this._menuTidyUp.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D2)));
            this._menuTidyUp.Size = new System.Drawing.Size(236, 22);
            this._menuTidyUp.Text = "整理(&C)";
            this._menuTidyUp.ToolTipText = "数式の各項を整理します。\r\n(例) (x * x + 1)(x + x + 1) → (1 + x^2)*(1 + 2*x)";
            this._menuTidyUp.Click += new System.EventHandler(this._menuTidyUp_Click);
            // 
            // _menuSimplify
            // 
            this._menuSimplify.Name = "_menuSimplify";
            this._menuSimplify.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D3)));
            this._menuSimplify.Size = new System.Drawing.Size(236, 22);
            this._menuSimplify.Text = "展開と整理(&S)";
            this._menuSimplify.ToolTipText = "数式を展開の上、整理します。\r\n(例) (x * x + 1)(x + x + 1) → 1 + x^2 + 2*x + 2*x^3";
            this._menuSimplify.Click += new System.EventHandler(this._menuSimplify_Click);
            // 
            // _menuFactorize
            // 
            this._menuFactorize.Name = "_menuFactorize";
            this._menuFactorize.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D4)));
            this._menuFactorize.Size = new System.Drawing.Size(236, 22);
            this._menuFactorize.Text = "因数分解(&F)";
            this._menuFactorize.ToolTipText = "数式を因数分解します（変数の多い式や次数の高い式では、処理に時間がかかることがあります）。\r\n(例) 2*x^3 + x^2 + 2*x + 1 → (2*x +" +
    " 1)*(1 + x^2)";
            this._menuFactorize.Click += new System.EventHandler(this._menuFactorize_Click);
            // 
            // _menuCollectAbout
            // 
            this._menuCollectAbout.Name = "_menuCollectAbout";
            this._menuCollectAbout.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D5)));
            this._menuCollectAbout.Size = new System.Drawing.Size(236, 22);
            this._menuCollectAbout.Text = "指定変数について整理(&T)";
            this._menuCollectAbout.ToolTipText = "数式を変数について整理します。";
            this._menuCollectAbout.Click += new System.EventHandler(this._menuCollectAbout_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(284, 6);
            // 
            // _menuStartHotLoading
            // 
            this._menuStartHotLoading.Name = "_menuStartHotLoading";
            this._menuStartHotLoading.Size = new System.Drawing.Size(287, 22);
            this._menuStartHotLoading.Text = "入力ファイルの同期(&L)";
            this._menuStartHotLoading.Click += new System.EventHandler(this._menuStartHotLoading_Click);
            // 
            // _menuInsertHelp
            // 
            this._menuInsertHelp.Name = "_menuInsertHelp";
            this._menuInsertHelp.Size = new System.Drawing.Size(287, 22);
            this._menuInsertHelp.Text = "ヘルプの挿入(&H)";
            this._menuInsertHelp.ToolTipText = "計算機の簡易な説明文を末尾に挿入します。";
            this._menuInsertHelp.Click += new System.EventHandler(this._menuInsertHelp_Click);
            // 
            // _splitContainer
            // 
            this._splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._splitContainer.Location = new System.Drawing.Point(0, 0);
            this._splitContainer.Name = "_splitContainer";
            // 
            // _splitContainer.Panel1
            // 
            this._splitContainer.Panel1.Controls.Add(this._panelHotLoading);
            this._splitContainer.Panel1.Controls.Add(this._panelFind);
            this._splitContainer.Panel1.Controls.Add(this._inputTextBox);
            this._splitContainer.Panel1.Controls.Add(this._btnAllDelete);
            // 
            // _splitContainer.Panel2
            // 
            this._splitContainer.Panel2.Controls.Add(this._panelHotSaving);
            this._splitContainer.Panel2.Controls.Add(this._resultTextBox);
            this._splitContainer.Size = new System.Drawing.Size(683, 440);
            this._splitContainer.SplitterDistance = 432;
            this._splitContainer.TabIndex = 17;
            // 
            // _panelHotLoading
            // 
            this._panelHotLoading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._panelHotLoading.BackColor = System.Drawing.Color.White;
            this._panelHotLoading.Controls.Add(this._btnSaveSync);
            this._panelHotLoading.Controls.Add(this._btnStopHotLoading);
            this._panelHotLoading.Controls.Add(this._textBoxHotReloadingPath);
            this._panelHotLoading.Controls.Add(this._picHotReloading);
            this._panelHotLoading.Location = new System.Drawing.Point(-5, 411);
            this._panelHotLoading.Name = "_panelHotLoading";
            this._panelHotLoading.Size = new System.Drawing.Size(437, 29);
            this._panelHotLoading.TabIndex = 24;
            this._panelHotLoading.Visible = false;
            // 
            // _btnSaveSync
            // 
            this._btnSaveSync.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnSaveSync.BackColor = System.Drawing.Color.White;
            this._btnSaveSync.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnSaveSync.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnSaveSync.DownMove = 1;
            this._btnSaveSync.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Save;
            this._btnSaveSync.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Save_Unfocus_mini;
            this._btnSaveSync.Location = new System.Drawing.Point(386, 1);
            this._btnSaveSync.Name = "_btnSaveSync";
            this._btnSaveSync.Size = new System.Drawing.Size(26, 26);
            this._btnSaveSync.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnSaveSync.TabIndex = 18;
            this._btnSaveSync.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnSaveSync, "編集内容を上書保存");
            this._btnSaveSync.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Save_Unfocus_mini;
            this._btnSaveSync.Click += new System.EventHandler(this._btnSaveSync_Click);
            // 
            // _btnStopHotLoading
            // 
            this._btnStopHotLoading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnStopHotLoading.BackColor = System.Drawing.Color.White;
            this._btnStopHotLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnStopHotLoading.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnStopHotLoading.DownMove = 1;
            this._btnStopHotLoading.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort;
            this._btnStopHotLoading.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort_Unfocus;
            this._btnStopHotLoading.Location = new System.Drawing.Point(412, 4);
            this._btnStopHotLoading.Name = "_btnStopHotLoading";
            this._btnStopHotLoading.Size = new System.Drawing.Size(22, 22);
            this._btnStopHotLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnStopHotLoading.TabIndex = 30;
            this._btnStopHotLoading.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnStopHotLoading, "自動同期の停止");
            this._btnStopHotLoading.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort_Unfocus;
            this._btnStopHotLoading.Click += new System.EventHandler(this._btnStopHotLoading_Click);
            // 
            // _textBoxHotReloadingPath
            // 
            this._textBoxHotReloadingPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxHotReloadingPath.BackColor = System.Drawing.Color.White;
            this._textBoxHotReloadingPath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._textBoxHotReloadingPath.ForeColor = System.Drawing.Color.Firebrick;
            this._textBoxHotReloadingPath.Location = new System.Drawing.Point(28, 9);
            this._textBoxHotReloadingPath.Name = "_textBoxHotReloadingPath";
            this._textBoxHotReloadingPath.ReadOnly = true;
            this._textBoxHotReloadingPath.Size = new System.Drawing.Size(350, 12);
            this._textBoxHotReloadingPath.TabIndex = 19;
            this._textBoxHotReloadingPath.Text = "C:\\\\Test";
            // 
            // _picHotReloading
            // 
            this._picHotReloading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._picHotReloading.Image = global::GoodSeat.Clapte.Properties.Resources.SpinnerBlueSmall;
            this._picHotReloading.Location = new System.Drawing.Point(6, 6);
            this._picHotReloading.Name = "_picHotReloading";
            this._picHotReloading.Size = new System.Drawing.Size(18, 18);
            this._picHotReloading.TabIndex = 18;
            this._picHotReloading.TabStop = false;
            this._toolTipHelp.SetToolTip(this._picHotReloading, "自動同期中…");
            // 
            // _panelFind
            // 
            this._panelFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._panelFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._panelFind.Controls.Add(this._btnToggleFindPanelPosition);
            this._panelFind.Controls.Add(this.pictureBox3);
            this._panelFind.Controls.Add(this._btnReplaceAll);
            this._panelFind.Controls.Add(this._btnHideFindPanel);
            this._panelFind.Controls.Add(this._btnToggleFindUseRegex);
            this._panelFind.Controls.Add(this._btnToggleFindMatchCase);
            this._panelFind.Controls.Add(this._labelReplace);
            this._panelFind.Controls.Add(this._labelFind);
            this._panelFind.Controls.Add(this._btnReplaceAndPrev);
            this._panelFind.Controls.Add(this._btnReplaceAndNext);
            this._panelFind.Controls.Add(this._btnFindPrev);
            this._panelFind.Controls.Add(this._btnFindNext);
            this._panelFind.Controls.Add(this.pictureBox2);
            this._panelFind.Controls.Add(this.pictureBox1);
            this._panelFind.Controls.Add(this._textBoxReplace);
            this._panelFind.Controls.Add(this._textBoxFind);
            this._panelFind.Location = new System.Drawing.Point(212, -1);
            this._panelFind.Name = "_panelFind";
            this._panelFind.Size = new System.Drawing.Size(220, 51);
            this._panelFind.TabIndex = 23;
            this._panelFind.Visible = false;
            // 
            // _btnToggleFindPanelPosition
            // 
            this._btnToggleFindPanelPosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnToggleFindPanelPosition.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnToggleFindPanelPosition.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnToggleFindPanelPosition.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnToggleFindPanelPosition.DownMove = 1;
            this._btnToggleFindPanelPosition.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_MoveFindPanel;
            this._btnToggleFindPanelPosition.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_MoveFindPanel_Unfocus;
            this._btnToggleFindPanelPosition.Location = new System.Drawing.Point(197, 26);
            this._btnToggleFindPanelPosition.Name = "_btnToggleFindPanelPosition";
            this._btnToggleFindPanelPosition.Size = new System.Drawing.Size(22, 22);
            this._btnToggleFindPanelPosition.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnToggleFindPanelPosition.TabIndex = 39;
            this._btnToggleFindPanelPosition.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnToggleFindPanelPosition, "検索パネルを移動");
            this._btnToggleFindPanelPosition.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_MoveFindPanel_Unfocus;
            this._btnToggleFindPanelPosition.Click += new System.EventHandler(this._btnToggleFindPanelPosition_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Silver;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.pictureBox3.Location = new System.Drawing.Point(0, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(5, 48);
            this.pictureBox3.TabIndex = 38;
            this.pictureBox3.TabStop = false;
            // 
            // _btnReplaceAll
            // 
            this._btnReplaceAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnReplaceAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnReplaceAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnReplaceAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnReplaceAll.DownMove = 1;
            this._btnReplaceAll.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAll;
            this._btnReplaceAll.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAll_Unfocus;
            this._btnReplaceAll.Location = new System.Drawing.Point(164, 26);
            this._btnReplaceAll.Name = "_btnReplaceAll";
            this._btnReplaceAll.Size = new System.Drawing.Size(22, 22);
            this._btnReplaceAll.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnReplaceAll.TabIndex = 37;
            this._btnReplaceAll.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnReplaceAll, "全て置換");
            this._btnReplaceAll.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAll_Unfocus;
            this._btnReplaceAll.Click += new System.EventHandler(this._btnReplaceAll_Click);
            this._btnReplaceAll.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // _btnHideFindPanel
            // 
            this._btnHideFindPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnHideFindPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnHideFindPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnHideFindPanel.DownMove = 1;
            this._btnHideFindPanel.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort;
            this._btnHideFindPanel.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort_Unfocus;
            this._btnHideFindPanel.Location = new System.Drawing.Point(197, 1);
            this._btnHideFindPanel.Name = "_btnHideFindPanel";
            this._btnHideFindPanel.Size = new System.Drawing.Size(22, 22);
            this._btnHideFindPanel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnHideFindPanel.TabIndex = 29;
            this._btnHideFindPanel.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnHideFindPanel, "検索パネルを閉じる");
            this._btnHideFindPanel.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort_Unfocus;
            this._btnHideFindPanel.Click += new System.EventHandler(this._btnHideFindPanel_Click);
            this._btnHideFindPanel.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // _btnToggleFindUseRegex
            // 
            this._btnToggleFindUseRegex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnToggleFindUseRegex.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnToggleFindUseRegex.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnToggleFindUseRegex.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnToggleFindUseRegex.DownMove = 1;
            this._btnToggleFindUseRegex.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_UseRegex;
            this._btnToggleFindUseRegex.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_UseRegex_Unfocus;
            this._btnToggleFindUseRegex.Location = new System.Drawing.Point(179, 1);
            this._btnToggleFindUseRegex.Name = "_btnToggleFindUseRegex";
            this._btnToggleFindUseRegex.Size = new System.Drawing.Size(22, 22);
            this._btnToggleFindUseRegex.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnToggleFindUseRegex.TabIndex = 36;
            this._btnToggleFindUseRegex.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnToggleFindUseRegex, "正規表現を使用");
            this._btnToggleFindUseRegex.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_UseRegex_Unfocus;
            this._btnToggleFindUseRegex.Click += new System.EventHandler(this._btnToggleFindUseRegex_Click);
            this._btnToggleFindUseRegex.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // _btnToggleFindMatchCase
            // 
            this._btnToggleFindMatchCase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnToggleFindMatchCase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnToggleFindMatchCase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnToggleFindMatchCase.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnToggleFindMatchCase.DownMove = 1;
            this._btnToggleFindMatchCase.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_MatchCase;
            this._btnToggleFindMatchCase.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_MatchCase_Unfocus;
            this._btnToggleFindMatchCase.Location = new System.Drawing.Point(160, 1);
            this._btnToggleFindMatchCase.Name = "_btnToggleFindMatchCase";
            this._btnToggleFindMatchCase.Size = new System.Drawing.Size(22, 22);
            this._btnToggleFindMatchCase.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnToggleFindMatchCase.TabIndex = 35;
            this._btnToggleFindMatchCase.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnToggleFindMatchCase, "大文字/小文字の区別");
            this._btnToggleFindMatchCase.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_MatchCase_Unfocus;
            this._btnToggleFindMatchCase.Click += new System.EventHandler(this._btnToggleFindMatchCase_Click);
            this._btnToggleFindMatchCase.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // _labelReplace
            // 
            this._labelReplace.AutoSize = true;
            this._labelReplace.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._labelReplace.ForeColor = System.Drawing.Color.DarkGray;
            this._labelReplace.Location = new System.Drawing.Point(12, 31);
            this._labelReplace.Name = "_labelReplace";
            this._labelReplace.Size = new System.Drawing.Size(35, 12);
            this._labelReplace.TabIndex = 34;
            this._labelReplace.Text = "置換...";
            this._labelReplace.Click += new System.EventHandler(this._labelReplace_Click);
            // 
            // _labelFind
            // 
            this._labelFind.AutoSize = true;
            this._labelFind.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._labelFind.ForeColor = System.Drawing.Color.DarkGray;
            this._labelFind.Location = new System.Drawing.Point(11, 7);
            this._labelFind.Name = "_labelFind";
            this._labelFind.Size = new System.Drawing.Size(35, 12);
            this._labelFind.TabIndex = 33;
            this._labelFind.Text = "検索...";
            this._labelFind.Click += new System.EventHandler(this._labelFind_Click);
            // 
            // _btnReplaceAndPrev
            // 
            this._btnReplaceAndPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnReplaceAndPrev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnReplaceAndPrev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnReplaceAndPrev.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnReplaceAndPrev.DownMove = 1;
            this._btnReplaceAndPrev.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAndPrev;
            this._btnReplaceAndPrev.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAndPrev_Unfocus;
            this._btnReplaceAndPrev.Location = new System.Drawing.Point(140, 26);
            this._btnReplaceAndPrev.Name = "_btnReplaceAndPrev";
            this._btnReplaceAndPrev.Size = new System.Drawing.Size(22, 22);
            this._btnReplaceAndPrev.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnReplaceAndPrev.TabIndex = 32;
            this._btnReplaceAndPrev.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnReplaceAndPrev, "置換して上を検索");
            this._btnReplaceAndPrev.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAndPrev_Unfocus;
            this._btnReplaceAndPrev.Click += new System.EventHandler(this._btnReplaceAndPrev_Click);
            this._btnReplaceAndPrev.DoubleClick += new System.EventHandler(this._btnReplaceAndPrev_Click);
            this._btnReplaceAndPrev.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // _btnReplaceAndNext
            // 
            this._btnReplaceAndNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnReplaceAndNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnReplaceAndNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnReplaceAndNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnReplaceAndNext.DownMove = 1;
            this._btnReplaceAndNext.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAndNext;
            this._btnReplaceAndNext.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAndNext_Unfocus;
            this._btnReplaceAndNext.Location = new System.Drawing.Point(120, 26);
            this._btnReplaceAndNext.Name = "_btnReplaceAndNext";
            this._btnReplaceAndNext.Size = new System.Drawing.Size(22, 22);
            this._btnReplaceAndNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnReplaceAndNext.TabIndex = 31;
            this._btnReplaceAndNext.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnReplaceAndNext, "置換して下を検索");
            this._btnReplaceAndNext.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ReplaceAndNext_Unfocus;
            this._btnReplaceAndNext.Click += new System.EventHandler(this._btnReplaceAndNext_Click);
            this._btnReplaceAndNext.DoubleClick += new System.EventHandler(this._btnReplaceAndNext_Click);
            this._btnReplaceAndNext.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // _btnFindPrev
            // 
            this._btnFindPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnFindPrev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnFindPrev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnFindPrev.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnFindPrev.DownMove = 1;
            this._btnFindPrev.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_FindPrev;
            this._btnFindPrev.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_FindPrev_Unfocus;
            this._btnFindPrev.Location = new System.Drawing.Point(140, 1);
            this._btnFindPrev.Name = "_btnFindPrev";
            this._btnFindPrev.Size = new System.Drawing.Size(22, 22);
            this._btnFindPrev.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnFindPrev.TabIndex = 28;
            this._btnFindPrev.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnFindPrev, "上を検索");
            this._btnFindPrev.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_FindPrev_Unfocus;
            this._btnFindPrev.Click += new System.EventHandler(this._btnFindPrev_Click);
            this._btnFindPrev.DoubleClick += new System.EventHandler(this._btnFindPrev_Click);
            this._btnFindPrev.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // _btnFindNext
            // 
            this._btnFindNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnFindNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this._btnFindNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnFindNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnFindNext.DownMove = 1;
            this._btnFindNext.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_FindNext;
            this._btnFindNext.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_FindNext_Unfocus;
            this._btnFindNext.Location = new System.Drawing.Point(120, 1);
            this._btnFindNext.Name = "_btnFindNext";
            this._btnFindNext.Size = new System.Drawing.Size(22, 22);
            this._btnFindNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnFindNext.TabIndex = 27;
            this._btnFindNext.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnFindNext, "下を検索");
            this._btnFindNext.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_FindNext_Unfocus;
            this._btnFindNext.Click += new System.EventHandler(this._btnFindNext_Click);
            this._btnFindNext.DoubleClick += new System.EventHandler(this._btnFindNext_Click);
            this._btnFindNext.MouseEnter += new System.EventHandler(this._btnFindNext_MouseEnter);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gray;
            this.pictureBox2.Location = new System.Drawing.Point(8, 46);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(110, 1);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gray;
            this.pictureBox1.Location = new System.Drawing.Point(8, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(110, 1);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // _textBoxReplace
            // 
            this._textBoxReplace.BackColor = System.Drawing.SystemColors.Window;
            this._textBoxReplace.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._textBoxReplace.Location = new System.Drawing.Point(10, 31);
            this._textBoxReplace.Name = "_textBoxReplace";
            this._textBoxReplace.Size = new System.Drawing.Size(100, 12);
            this._textBoxReplace.TabIndex = 1;
            this._textBoxReplace.TextChanged += new System.EventHandler(this._textBoxReplace_TextChanged);
            this._textBoxReplace.KeyUp += new System.Windows.Forms.KeyEventHandler(this._textBoxReplace_KeyUp);
            // 
            // _textBoxFind
            // 
            this._textBoxFind.BackColor = System.Drawing.SystemColors.Window;
            this._textBoxFind.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._textBoxFind.Location = new System.Drawing.Point(10, 7);
            this._textBoxFind.Name = "_textBoxFind";
            this._textBoxFind.Size = new System.Drawing.Size(100, 12);
            this._textBoxFind.TabIndex = 0;
            this._textBoxFind.TextChanged += new System.EventHandler(this._textBoxFind_TextChanged);
            this._textBoxFind.KeyUp += new System.Windows.Forms.KeyEventHandler(this._textBoxFind_KeyUp);
            // 
            // _btnAllDelete
            // 
            this._btnAllDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnAllDelete.BackColor = System.Drawing.Color.White;
            this._btnAllDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnAllDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnAllDelete.DownMove = 1;
            this._btnAllDelete.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ClearFilter;
            this._btnAllDelete.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_ClearFilter_Unfocus;
            this._btnAllDelete.Location = new System.Drawing.Point(417, 1);
            this._btnAllDelete.Name = "_btnAllDelete";
            this._btnAllDelete.Size = new System.Drawing.Size(15, 15);
            this._btnAllDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._btnAllDelete.TabIndex = 22;
            this._btnAllDelete.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnAllDelete, "テキストを全削除");
            this._btnAllDelete.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ClearFilter_Unfocus;
            this._btnAllDelete.Click += new System.EventHandler(this._btnAllDelete_Click);
            // 
            // _panelHotSaving
            // 
            this._panelHotSaving.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._panelHotSaving.BackColor = System.Drawing.Color.WhiteSmoke;
            this._panelHotSaving.Controls.Add(this._btnStopHotSaving);
            this._panelHotSaving.Controls.Add(this._textBoxHotSavingPath);
            this._panelHotSaving.Controls.Add(this._picHotSaving);
            this._panelHotSaving.Location = new System.Drawing.Point(0, 411);
            this._panelHotSaving.Name = "_panelHotSaving";
            this._panelHotSaving.Size = new System.Drawing.Size(230, 29);
            this._panelHotSaving.TabIndex = 25;
            this._panelHotSaving.Visible = false;
            // 
            // _btnStopHotSaving
            // 
            this._btnStopHotSaving.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnStopHotSaving.BackColor = System.Drawing.Color.WhiteSmoke;
            this._btnStopHotSaving.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnStopHotSaving.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnStopHotSaving.DownMove = 1;
            this._btnStopHotSaving.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort;
            this._btnStopHotSaving.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort_Unfocus;
            this._btnStopHotSaving.Location = new System.Drawing.Point(205, 4);
            this._btnStopHotSaving.Name = "_btnStopHotSaving";
            this._btnStopHotSaving.Size = new System.Drawing.Size(22, 22);
            this._btnStopHotSaving.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnStopHotSaving.TabIndex = 30;
            this._btnStopHotSaving.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnStopHotSaving, "自動出力の停止");
            this._btnStopHotSaving.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Abort_Unfocus;
            this._btnStopHotSaving.Click += new System.EventHandler(this._menuToggleHotSaving_Click);
            // 
            // _textBoxHotSavingPath
            // 
            this._textBoxHotSavingPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._textBoxHotSavingPath.BackColor = System.Drawing.Color.WhiteSmoke;
            this._textBoxHotSavingPath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._textBoxHotSavingPath.ForeColor = System.Drawing.Color.Firebrick;
            this._textBoxHotSavingPath.Location = new System.Drawing.Point(28, 9);
            this._textBoxHotSavingPath.Name = "_textBoxHotSavingPath";
            this._textBoxHotSavingPath.ReadOnly = true;
            this._textBoxHotSavingPath.Size = new System.Drawing.Size(172, 12);
            this._textBoxHotSavingPath.TabIndex = 19;
            this._textBoxHotSavingPath.Text = "C:\\\\Test";
            // 
            // _picHotSaving
            // 
            this._picHotSaving.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._picHotSaving.Image = global::GoodSeat.Clapte.Properties.Resources.SpinnerBlueSmall;
            this._picHotSaving.Location = new System.Drawing.Point(6, 6);
            this._picHotSaving.Name = "_picHotSaving";
            this._picHotSaving.Size = new System.Drawing.Size(18, 18);
            this._picHotSaving.TabIndex = 18;
            this._picHotSaving.TabStop = false;
            this._toolTipHelp.SetToolTip(this._picHotSaving, "自動出力中…");
            // 
            // _resultTextBox
            // 
            this._resultTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this._resultTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._resultTextBox.ContextMenuStrip = this._contextMenuResult;
            this._resultTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._resultTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._resultTextBox.DrawingOption = Sgry.Azuki.DrawingOption.HighlightsMatchedBracket;
            this._resultTextBox.DrawsEolCode = false;
            this._resultTextBox.DrawsFullWidthSpace = false;
            this._resultTextBox.DrawsTab = false;
            this._resultTextBox.FirstVisibleLine = 0;
            this._resultTextBox.Font = new System.Drawing.Font("HGｺﾞｼｯｸM", 9F);
            fontInfo2.Name = "HGｺﾞｼｯｸM";
            fontInfo2.Size = 9;
            fontInfo2.Style = System.Drawing.FontStyle.Regular;
            this._resultTextBox.FontInfo = fontInfo2;
            this._resultTextBox.ForeColor = System.Drawing.Color.Black;
            this._resultTextBox.HighlightsCurrentLine = false;
            this._resultTextBox.IsReadOnly = true;
            this._resultTextBox.Location = new System.Drawing.Point(0, 0);
            this._resultTextBox.Name = "_resultTextBox";
            this._resultTextBox.ScrollPos = new System.Drawing.Point(0, 0);
            this._resultTextBox.ScrollsBeyondLastLine = false;
            this._resultTextBox.ShowsDirtBar = false;
            this._resultTextBox.ShowsHScrollBar = false;
            this._resultTextBox.ShowsLineNumber = false;
            this._resultTextBox.Size = new System.Drawing.Size(247, 440);
            this._resultTextBox.TabIndex = 17;
            this._resultTextBox.ViewWidth = 4097;
            this._resultTextBox.CaretMoved += new System.EventHandler(this._resultTextBox_CaretMoved);
            this._resultTextBox.VScroll += new System.EventHandler(this._resultTextBox_VScroll);
            this._resultTextBox.FontChanged += new System.EventHandler(this._inputTextBox_FontChanged);
            this._resultTextBox.Enter += new System.EventHandler(this._inputTextBox_Enter);
            this._resultTextBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this._inputTextBox_MouseMove);
            // 
            // _contextMenuResult
            // 
            this._contextMenuResult.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._menuCopyResult,
            this.toolStripSeparator5,
            this._menuSelectAllResult,
            this.toolStripSeparator6,
            this._menuFindAndReplaceResult,
            this._menuJumpDefineResult,
            this._menuAddUserDefineResult,
            this._menuVisibleDeformHistoryResult,
            this.toolStripSeparator15,
            this._menuToggleHotSaving});
            this._contextMenuResult.Name = "_contextMenuEdit";
            this._contextMenuResult.Size = new System.Drawing.Size(206, 176);
            this._contextMenuResult.Opening += new System.ComponentModel.CancelEventHandler(this._contextMenuEdit_Opening);
            // 
            // _menuCopyResult
            // 
            this._menuCopyResult.Name = "_menuCopyResult";
            this._menuCopyResult.Size = new System.Drawing.Size(205, 22);
            this._menuCopyResult.Text = "コピー(&C)";
            this._menuCopyResult.Click += new System.EventHandler(this._menuCopyResult_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(202, 6);
            // 
            // _menuSelectAllResult
            // 
            this._menuSelectAllResult.Name = "_menuSelectAllResult";
            this._menuSelectAllResult.Size = new System.Drawing.Size(205, 22);
            this._menuSelectAllResult.Text = "すべて選択(&A)";
            this._menuSelectAllResult.Click += new System.EventHandler(this._menuSelectAllResult_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(202, 6);
            // 
            // _menuFindAndReplaceResult
            // 
            this._menuFindAndReplaceResult.Name = "_menuFindAndReplaceResult";
            this._menuFindAndReplaceResult.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._menuFindAndReplaceResult.Size = new System.Drawing.Size(205, 22);
            this._menuFindAndReplaceResult.Text = "検索と置換(&F)";
            this._menuFindAndReplaceResult.ToolTipText = "検索/置換パネルを表示します。";
            this._menuFindAndReplaceResult.Click += new System.EventHandler(this._menuFindAndReplace_Click);
            // 
            // _menuJumpDefineResult
            // 
            this._menuJumpDefineResult.Name = "_menuJumpDefineResult";
            this._menuJumpDefineResult.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this._menuJumpDefineResult.Size = new System.Drawing.Size(205, 22);
            this._menuJumpDefineResult.Text = "定義を参照(&J)";
            this._menuJumpDefineResult.ToolTipText = "カーソル位置にある定数/関数/単位の定義位置にジャンプします。";
            this._menuJumpDefineResult.Click += new System.EventHandler(this._menuJumpDefineResult_Click);
            // 
            // _menuAddUserDefineResult
            // 
            this._menuAddUserDefineResult.Name = "_menuAddUserDefineResult";
            this._menuAddUserDefineResult.Size = new System.Drawing.Size(205, 22);
            this._menuAddUserDefineResult.Text = "ユーザー定義に登録(&G)";
            this._menuAddUserDefineResult.Visible = false;
            this._menuAddUserDefineResult.Click += new System.EventHandler(this._menuAddUserDefineResult_Click);
            // 
            // _menuVisibleDeformHistoryResult
            // 
            this._menuVisibleDeformHistoryResult.Name = "_menuVisibleDeformHistoryResult";
            this._menuVisibleDeformHistoryResult.Size = new System.Drawing.Size(205, 22);
            this._menuVisibleDeformHistoryResult.Text = "計算過程の表示(&D)";
            this._menuVisibleDeformHistoryResult.ToolTipText = "計算過程の表示/非表示を切り替えます。";
            this._menuVisibleDeformHistoryResult.Click += new System.EventHandler(this._menuVisibleDeformHistory_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(202, 6);
            // 
            // _menuToggleHotSaving
            // 
            this._menuToggleHotSaving.Name = "_menuToggleHotSaving";
            this._menuToggleHotSaving.Size = new System.Drawing.Size(205, 22);
            this._menuToggleHotSaving.Text = "結果テキストの自動出力(&S)";
            this._menuToggleHotSaving.Click += new System.EventHandler(this._menuToggleHotSaving_Click);
            // 
            // _treeViewHistory
            // 
            this._treeViewHistory.BackColor = System.Drawing.Color.WhiteSmoke;
            this._treeViewHistory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._treeViewHistory.ContextMenuStrip = this._contextMenuHistory;
            this._treeViewHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this._treeViewHistory.HideSelection = false;
            this._treeViewHistory.Indent = 30;
            this._treeViewHistory.LineColor = System.Drawing.Color.Gray;
            this._treeViewHistory.Location = new System.Drawing.Point(0, 0);
            this._treeViewHistory.Name = "_treeViewHistory";
            this._treeViewHistory.ShowLines = false;
            this._treeViewHistory.Size = new System.Drawing.Size(150, 46);
            this._treeViewHistory.TabIndex = 23;
            // 
            // _contextMenuHistory
            // 
            this._contextMenuHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._menuHideDeformHistory,
            this.toolStripSeparator9,
            this._menuExpandHistory,
            this._menuFoldHistory});
            this._contextMenuHistory.Name = "_contextMenuHistory";
            this._contextMenuHistory.Size = new System.Drawing.Size(215, 76);
            // 
            // _menuHideDeformHistory
            // 
            this._menuHideDeformHistory.Name = "_menuHideDeformHistory";
            this._menuHideDeformHistory.Size = new System.Drawing.Size(214, 22);
            this._menuHideDeformHistory.Text = "計算過程の表示を閉じる(&H)";
            this._menuHideDeformHistory.Click += new System.EventHandler(this._menuHideDeformHistory_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(211, 6);
            // 
            // _menuExpandHistory
            // 
            this._menuExpandHistory.Name = "_menuExpandHistory";
            this._menuExpandHistory.Size = new System.Drawing.Size(214, 22);
            this._menuExpandHistory.Text = "計算過程を全て展開(&E)";
            this._menuExpandHistory.Click += new System.EventHandler(this._menuExpandHistory_Click);
            // 
            // _menuFoldHistory
            // 
            this._menuFoldHistory.Name = "_menuFoldHistory";
            this._menuFoldHistory.Size = new System.Drawing.Size(214, 22);
            this._menuFoldHistory.Text = "計算過程を全て折りたたむ(&F)";
            this._menuFoldHistory.Click += new System.EventHandler(this._menuFoldHistory_Click);
            // 
            // _picStatus
            // 
            this._picStatus.Image = global::GoodSeat.Clapte.Properties.Resources.status_anim;
            this._picStatus.Location = new System.Drawing.Point(93, 12);
            this._picStatus.Name = "_picStatus";
            this._picStatus.Size = new System.Drawing.Size(47, 16);
            this._picStatus.TabIndex = 17;
            this._picStatus.TabStop = false;
            this._picStatus.Visible = false;
            this._picStatus.VisibleChanged += new System.EventHandler(this._picStatus_VisibleChanged);
            // 
            // _btnAbort
            // 
            this._btnAbort.BackColor = System.Drawing.Color.White;
            this._btnAbort.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnAbort.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnAbort.DownMove = 1;
            this._btnAbort.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ClearFilter;
            this._btnAbort.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_ClearFilter_Unfocus;
            this._btnAbort.Location = new System.Drawing.Point(73, 10);
            this._btnAbort.Name = "_btnAbort";
            this._btnAbort.Size = new System.Drawing.Size(15, 15);
            this._btnAbort.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._btnAbort.TabIndex = 20;
            this._btnAbort.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnAbort, "評価の中止");
            this._btnAbort.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_ClearFilter_Unfocus;
            this._btnAbort.Visible = false;
            this._btnAbort.Click += new System.EventHandler(this._btnAbort_Click);
            // 
            // _btnLoad
            // 
            this._btnLoad.BackColor = System.Drawing.Color.White;
            this._btnLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnLoad.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnLoad.DownMove = 1;
            this._btnLoad.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Open;
            this._btnLoad.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Open_Unfocus_mini;
            this._btnLoad.Location = new System.Drawing.Point(29, 3);
            this._btnLoad.Name = "_btnLoad";
            this._btnLoad.Size = new System.Drawing.Size(26, 26);
            this._btnLoad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnLoad.TabIndex = 19;
            this._btnLoad.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnLoad, "テキストファイルの読込");
            this._btnLoad.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Open_Unfocus_mini;
            this._btnLoad.Click += new System.EventHandler(this._btnLoad_Click);
            this._btnLoad.MouseEnter += new System.EventHandler(this._btnSave_MouseEnter);
            // 
            // _btnSave
            // 
            this._btnSave.BackColor = System.Drawing.Color.White;
            this._btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnSave.DownMove = 1;
            this._btnSave.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Save;
            this._btnSave.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Save_Unfocus_mini;
            this._btnSave.Location = new System.Drawing.Point(5, 3);
            this._btnSave.Name = "_btnSave";
            this._btnSave.Size = new System.Drawing.Size(26, 26);
            this._btnSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnSave.TabIndex = 18;
            this._btnSave.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnSave, "テキストファイルに保存");
            this._btnSave.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Save_Unfocus_mini;
            this._btnSave.Click += new System.EventHandler(this._btnSave_Click);
            this._btnSave.MouseEnter += new System.EventHandler(this._btnSave_MouseEnter);
            // 
            // _timerDelay
            // 
            this._timerDelay.Tick += new System.EventHandler(this._timerDelay_Tick);
            // 
            // _openFileDialog
            // 
            this._openFileDialog.DefaultExt = "txt";
            this._openFileDialog.Filter = "テキストファイル(*.txt)|*.txt|すべてのファイル|*.*";
            this._openFileDialog.InitialDirectory = "Notes";
            this._openFileDialog.Title = "テキストの読み込み";
            // 
            // _saveFileDialog
            // 
            this._saveFileDialog.DefaultExt = "txt";
            this._saveFileDialog.Filter = "入力テキスト(*.txt)|*.txt|すべてのファイル|*.*";
            this._saveFileDialog.InitialDirectory = "Notes";
            this._saveFileDialog.Title = "テキストの保存";
            // 
            // _btnSetting
            // 
            this._btnSetting.BackColor = System.Drawing.Color.White;
            this._btnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnSetting.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnSetting.DownMove = 1;
            this._btnSetting.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Setting;
            this._btnSetting.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Setting_Unfocus;
            this._btnSetting.Location = new System.Drawing.Point(48, 3);
            this._btnSetting.Name = "_btnSetting";
            this._btnSetting.Size = new System.Drawing.Size(26, 26);
            this._btnSetting.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnSetting.TabIndex = 21;
            this._btnSetting.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnSetting, "設定を開く");
            this._btnSetting.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Setting_Unfocus;
            this._btnSetting.Click += new System.EventHandler(this._btnSetting_Click);
            this._btnSetting.MouseEnter += new System.EventHandler(this._btnSave_MouseEnter);
            // 
            // _btnMinimize
            // 
            this._btnMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnMinimize.BackColor = System.Drawing.Color.White;
            this._btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnMinimize.DownMove = 1;
            this._btnMinimize.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Sub;
            this._btnMinimize.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Sub_Unfocus;
            this._btnMinimize.Location = new System.Drawing.Point(642, 14);
            this._btnMinimize.Name = "_btnMinimize";
            this._btnMinimize.Size = new System.Drawing.Size(18, 13);
            this._btnMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnMinimize.TabIndex = 22;
            this._btnMinimize.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnMinimize, "最小化");
            this._btnMinimize.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Sub_Unfocus;
            this._btnMinimize.Click += new System.EventHandler(this._btnMinimize_Click);
            // 
            // _splitContainerAll
            // 
            this._splitContainerAll.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._splitContainerAll.Location = new System.Drawing.Point(10, 32);
            this._splitContainerAll.Name = "_splitContainerAll";
            this._splitContainerAll.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // _splitContainerAll.Panel1
            // 
            this._splitContainerAll.Panel1.Controls.Add(this._splitContainer);
            // 
            // _splitContainerAll.Panel2
            // 
            this._splitContainerAll.Panel2.Controls.Add(this._treeViewHistory);
            this._splitContainerAll.Panel2Collapsed = true;
            this._splitContainerAll.Panel2MinSize = 0;
            this._splitContainerAll.Size = new System.Drawing.Size(683, 440);
            this._splitContainerAll.SplitterDistance = 296;
            this._splitContainerAll.TabIndex = 24;
            // 
            // _contextMenuHistoryNode
            // 
            this._contextMenuHistoryNode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._menuCopyFormulaInHistory,
            this.toolStripSeparator10,
            this._menuHideDeformHistoryNode,
            this.toolStripSeparator11,
            this._menuExpandHistoryThisFormula,
            this._menuFoldHistoryThisFormula,
            this._menuExpandHistoryNode,
            this._menuFoldHistoryNode});
            this._contextMenuHistoryNode.Name = "_contextMenuHistoryNode";
            this._contextMenuHistoryNode.Size = new System.Drawing.Size(267, 148);
            this._contextMenuHistoryNode.Opening += new System.ComponentModel.CancelEventHandler(this._contextMenuHistoryNode_Opening);
            // 
            // _menuCopyFormulaInHistory
            // 
            this._menuCopyFormulaInHistory.Name = "_menuCopyFormulaInHistory";
            this._menuCopyFormulaInHistory.Size = new System.Drawing.Size(266, 22);
            this._menuCopyFormulaInHistory.Text = "数式をコピー(&C)";
            this._menuCopyFormulaInHistory.Click += new System.EventHandler(this._menuCopyFormulaInHistory_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(263, 6);
            // 
            // _menuHideDeformHistoryNode
            // 
            this._menuHideDeformHistoryNode.Name = "_menuHideDeformHistoryNode";
            this._menuHideDeformHistoryNode.Size = new System.Drawing.Size(266, 22);
            this._menuHideDeformHistoryNode.Text = "計算過程の表示を閉じる(&H)";
            this._menuHideDeformHistoryNode.Click += new System.EventHandler(this._menuHideDeformHistory_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(263, 6);
            // 
            // _menuExpandHistoryThisFormula
            // 
            this._menuExpandHistoryThisFormula.Name = "_menuExpandHistoryThisFormula";
            this._menuExpandHistoryThisFormula.Size = new System.Drawing.Size(266, 22);
            this._menuExpandHistoryThisFormula.Text = "この数式の計算過程を全て展開(&D)";
            this._menuExpandHistoryThisFormula.Click += new System.EventHandler(this._menuExpandHistoryThisFormula_Click);
            // 
            // _menuFoldHistoryThisFormula
            // 
            this._menuFoldHistoryThisFormula.Name = "_menuFoldHistoryThisFormula";
            this._menuFoldHistoryThisFormula.Size = new System.Drawing.Size(266, 22);
            this._menuFoldHistoryThisFormula.Text = "この数式の計算過程を全て折りたたむ(&S)";
            this._menuFoldHistoryThisFormula.Click += new System.EventHandler(this._menuFoldHistoryThisFormula_Click);
            // 
            // _menuExpandHistoryNode
            // 
            this._menuExpandHistoryNode.Name = "_menuExpandHistoryNode";
            this._menuExpandHistoryNode.Size = new System.Drawing.Size(266, 22);
            this._menuExpandHistoryNode.Text = "計算過程を全て展開(&E)";
            this._menuExpandHistoryNode.Click += new System.EventHandler(this._menuExpandHistory_Click);
            // 
            // _menuFoldHistoryNode
            // 
            this._menuFoldHistoryNode.Name = "_menuFoldHistoryNode";
            this._menuFoldHistoryNode.Size = new System.Drawing.Size(266, 22);
            this._menuFoldHistoryNode.Text = "計算過程を全て折りたたむ(&F)";
            this._menuFoldHistoryNode.Click += new System.EventHandler(this._menuFoldHistory_Click);
            // 
            // _btnHelp
            // 
            this._btnHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnHelp.BackColor = System.Drawing.Color.White;
            this._btnHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnHelp.DownMove = 1;
            this._btnHelp.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Help;
            this._btnHelp.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Help_Unfocus;
            this._btnHelp.Location = new System.Drawing.Point(623, 8);
            this._btnHelp.Name = "_btnHelp";
            this._btnHelp.Size = new System.Drawing.Size(15, 15);
            this._btnHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnHelp.TabIndex = 25;
            this._btnHelp.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnHelp, "ヘルプの表示");
            this._btnHelp.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Help_Unfocus;
            this._btnHelp.Click += new System.EventHandler(this._btnHelp_Click);
            // 
            // _btnUpdate
            // 
            this._btnUpdate.BackColor = System.Drawing.Color.White;
            this._btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this._btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this._btnUpdate.DownMove = 1;
            this._btnUpdate.FocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Update;
            this._btnUpdate.Image = global::GoodSeat.Clapte.Properties.Resources.Icon_Update_Unfocus;
            this._btnUpdate.Location = new System.Drawing.Point(68, 3);
            this._btnUpdate.Name = "_btnUpdate";
            this._btnUpdate.Size = new System.Drawing.Size(26, 26);
            this._btnUpdate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._btnUpdate.TabIndex = 26;
            this._btnUpdate.TabStop = false;
            this._toolTipHelp.SetToolTip(this._btnUpdate, "再計算");
            this._btnUpdate.UnFocusImage = global::GoodSeat.Clapte.Properties.Resources.Icon_Update_Unfocus;
            this._btnUpdate.Click += new System.EventHandler(this._btnUpdate_Click);
            this._btnUpdate.MouseEnter += new System.EventHandler(this._btnSave_MouseEnter);
            // 
            // _toolTipFind
            // 
            this._toolTipFind.AutoPopDelay = 3000;
            this._toolTipFind.BackColor = System.Drawing.Color.White;
            this._toolTipFind.ForeColor = System.Drawing.Color.Firebrick;
            this._toolTipFind.InitialDelay = 0;
            this._toolTipFind.ReshowDelay = 10;
            this._toolTipFind.UseAnimation = false;
            this._toolTipFind.UseFading = false;
            // 
            // _expandMenuConvertUnit
            // 
            this._expandMenuConvertUnit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._txtBoxTargetUnit});
            this._expandMenuConvertUnit.Name = "_expandMenuConvertUnit";
            this._expandMenuConvertUnit.ShowImageMargin = false;
            this._expandMenuConvertUnit.Size = new System.Drawing.Size(136, 29);
            // 
            // _txtBoxTargetUnit
            // 
            this._txtBoxTargetUnit.Font = new System.Drawing.Font("Yu Gothic UI", 9F);
            this._txtBoxTargetUnit.ForeColor = System.Drawing.SystemColors.WindowText;
            this._txtBoxTargetUnit.Name = "_txtBoxTargetUnit";
            this._txtBoxTargetUnit.Size = new System.Drawing.Size(100, 23);
            this._txtBoxTargetUnit.ToolTipText = "換算の目標単位を指定します";
            this._txtBoxTargetUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this._txtBoxTargetUnit_KeyDown);
            this._txtBoxTargetUnit.TextChanged += new System.EventHandler(this._txtBoxTargetUnit_TextChanged);
            // 
            // _expandMenuExtractDefine
            // 
            this._expandMenuExtractDefine.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._txtBoxDefineConstantOfFunctionName});
            this._expandMenuExtractDefine.Name = "_expandMenuExtractDefine";
            this._expandMenuExtractDefine.ShowImageMargin = false;
            this._expandMenuExtractDefine.Size = new System.Drawing.Size(136, 29);
            // 
            // _txtBoxDefineConstantOfFunctionName
            // 
            this._txtBoxDefineConstantOfFunctionName.Font = new System.Drawing.Font("Yu Gothic UI", 9F);
            this._txtBoxDefineConstantOfFunctionName.Name = "_txtBoxDefineConstantOfFunctionName";
            this._txtBoxDefineConstantOfFunctionName.Size = new System.Drawing.Size(100, 23);
            this._txtBoxDefineConstantOfFunctionName.ToolTipText = "抽出した定義に付与する定数名/関数名を指定します";
            this._txtBoxDefineConstantOfFunctionName.KeyDown += new System.Windows.Forms.KeyEventHandler(this._txtBoxDefineConstantOfFunctionName_KeyDown);
            // 
            // _expandMenuSubstitute
            // 
            this._expandMenuSubstitute.Name = "_expandMenuSubstitute";
            this._expandMenuSubstitute.ShowImageMargin = false;
            this._expandMenuSubstitute.Size = new System.Drawing.Size(36, 4);
            // 
            // _expandMenuCollectAbout
            // 
            this._expandMenuCollectAbout.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._txtBoxCollectAbout});
            this._expandMenuCollectAbout.Name = "_expandMenuConvertUnit";
            this._expandMenuCollectAbout.ShowImageMargin = false;
            this._expandMenuCollectAbout.Size = new System.Drawing.Size(136, 29);
            // 
            // _txtBoxCollectAbout
            // 
            this._txtBoxCollectAbout.Font = new System.Drawing.Font("Yu Gothic UI", 9F);
            this._txtBoxCollectAbout.ForeColor = System.Drawing.SystemColors.WindowText;
            this._txtBoxCollectAbout.Name = "_txtBoxCollectAbout";
            this._txtBoxCollectAbout.Size = new System.Drawing.Size(100, 23);
            this._txtBoxCollectAbout.ToolTipText = "対象とする変数を選択します";
            this._txtBoxCollectAbout.KeyDown += new System.Windows.Forms.KeyEventHandler(this._txtBoxCollectAbout_KeyDown);
            this._txtBoxCollectAbout.TextChanged += new System.EventHandler(this._txtBoxCollectAbout_TextChanged);
            // 
            // _fileSystemWatcherHotLoading
            // 
            this._fileSystemWatcherHotLoading.EnableRaisingEvents = true;
            this._fileSystemWatcherHotLoading.SynchronizingObject = this;
            this._fileSystemWatcherHotLoading.Changed += new System.IO.FileSystemEventHandler(this._fileSystemWatcherHotLoading_Changed);
            this._fileSystemWatcherHotLoading.Deleted += new System.IO.FileSystemEventHandler(this._fileSystemWatcherHotLoading_Deleted);
            this._fileSystemWatcherHotLoading.Renamed += new System.IO.RenamedEventHandler(this._fileSystemWatcherHotLoading_Renamed);
            // 
            // _contextMenuEditOnHotLoading
            // 
            this._contextMenuEditOnHotLoading.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._menuCopyOnHotLoading,
            this.toolStripSeparator13,
            this._menuSelectAllOnHotLoading,
            this.toolStripSeparator14,
            this._menuFindOnHotLoading,
            this._menuVisibleDeformHistoryOnHotLoading,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11,
            this.toolStripSeparator12,
            this._menuStopHotLoading});
            this._contextMenuEditOnHotLoading.Name = "_contextMenuEdit";
            this._contextMenuEditOnHotLoading.Size = new System.Drawing.Size(215, 176);
            // 
            // _menuCopyOnHotLoading
            // 
            this._menuCopyOnHotLoading.Name = "_menuCopyOnHotLoading";
            this._menuCopyOnHotLoading.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._menuCopyOnHotLoading.Size = new System.Drawing.Size(214, 22);
            this._menuCopyOnHotLoading.Text = "コピー(&C)";
            this._menuCopyOnHotLoading.Click += new System.EventHandler(this._menuCopy_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(211, 6);
            // 
            // _menuSelectAllOnHotLoading
            // 
            this._menuSelectAllOnHotLoading.Name = "_menuSelectAllOnHotLoading";
            this._menuSelectAllOnHotLoading.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this._menuSelectAllOnHotLoading.Size = new System.Drawing.Size(214, 22);
            this._menuSelectAllOnHotLoading.Text = "すべて選択(&A)";
            this._menuSelectAllOnHotLoading.Click += new System.EventHandler(this._menuSelectAll_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(211, 6);
            // 
            // _menuFindOnHotLoading
            // 
            this._menuFindOnHotLoading.Name = "_menuFindOnHotLoading";
            this._menuFindOnHotLoading.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._menuFindOnHotLoading.Size = new System.Drawing.Size(214, 22);
            this._menuFindOnHotLoading.Text = "検索(&F)";
            this._menuFindOnHotLoading.ToolTipText = "検索パネルを表示します。";
            this._menuFindOnHotLoading.Click += new System.EventHandler(this._menuFindAndReplace_Click);
            // 
            // _menuVisibleDeformHistoryOnHotLoading
            // 
            this._menuVisibleDeformHistoryOnHotLoading.Name = "_menuVisibleDeformHistoryOnHotLoading";
            this._menuVisibleDeformHistoryOnHotLoading.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this._menuVisibleDeformHistoryOnHotLoading.Size = new System.Drawing.Size(214, 22);
            this._menuVisibleDeformHistoryOnHotLoading.Text = "計算過程の表示(&D)";
            this._menuVisibleDeformHistoryOnHotLoading.ToolTipText = "計算過程の表示/非表示を切り替えます。";
            this._menuVisibleDeformHistoryOnHotLoading.Click += new System.EventHandler(this._menuVisibleDeformHistory_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.toolStripMenuItem10.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItem10.Text = "定義を参照(&J)";
            this.toolStripMenuItem10.ToolTipText = "カーソル位置にある定数/関数/単位の定義位置にジャンプします。";
            this.toolStripMenuItem10.Click += new System.EventHandler(this._menuJumpDefine_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItem11.Text = "ユーザー定義に登録(&G)";
            this.toolStripMenuItem11.Visible = false;
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(211, 6);
            // 
            // _menuStopHotLoading
            // 
            this._menuStopHotLoading.Name = "_menuStopHotLoading";
            this._menuStopHotLoading.Size = new System.Drawing.Size(214, 22);
            this._menuStopHotLoading.Text = "ホットローディングの停止(&S)";
            this._menuStopHotLoading.ToolTipText = "ホットローディングを停止します。";
            this._menuStopHotLoading.Click += new System.EventHandler(this._btnStopHotLoading_Click);
            // 
            // FormOfClaptePad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 480);
            this.Controls.Add(this._btnUpdate);
            this.Controls.Add(this._btnHelp);
            this.Controls.Add(this._splitContainerAll);
            this.Controls.Add(this._btnMinimize);
            this.Controls.Add(this._btnSetting);
            this.Controls.Add(this._picStatus);
            this.Controls.Add(this._btnAbort);
            this.Controls.Add(this._btnLoad);
            this.Controls.Add(this._btnSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormOfClaptePad";
            this.ShowCancelButton = true;
            this.ShowOKButton = true;
            this.ShowOption = true;
            this.Text = "";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormOfClaptePad_FormClosing);
            this.Resize += new System.EventHandler(this.FormOfClaptePad_Resize);
            this.Controls.SetChildIndex(this._btnSave, 0);
            this.Controls.SetChildIndex(this._btnLoad, 0);
            this.Controls.SetChildIndex(this._btnAbort, 0);
            this.Controls.SetChildIndex(this._picStatus, 0);
            this.Controls.SetChildIndex(this._btnSetting, 0);
            this.Controls.SetChildIndex(this._btnMinimize, 0);
            this.Controls.SetChildIndex(this._splitContainerAll, 0);
            this.Controls.SetChildIndex(this._btnHelp, 0);
            this.Controls.SetChildIndex(this._btnUpdate, 0);
            this._contextMenuEdit.ResumeLayout(false);
            this._splitContainer.Panel1.ResumeLayout(false);
            this._splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._splitContainer)).EndInit();
            this._splitContainer.ResumeLayout(false);
            this._panelHotLoading.ResumeLayout(false);
            this._panelHotLoading.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._btnSaveSync)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnStopHotLoading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._picHotReloading)).EndInit();
            this._panelFind.ResumeLayout(false);
            this._panelFind.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._btnToggleFindPanelPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnReplaceAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnHideFindPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnToggleFindUseRegex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnToggleFindMatchCase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnReplaceAndPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnReplaceAndNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnFindPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnFindNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnAllDelete)).EndInit();
            this._panelHotSaving.ResumeLayout(false);
            this._panelHotSaving.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._btnStopHotSaving)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._picHotSaving)).EndInit();
            this._contextMenuResult.ResumeLayout(false);
            this._contextMenuHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._picStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnAbort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnMinimize)).EndInit();
            this._splitContainerAll.Panel1.ResumeLayout(false);
            this._splitContainerAll.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._splitContainerAll)).EndInit();
            this._splitContainerAll.ResumeLayout(false);
            this._contextMenuHistoryNode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._btnHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._btnUpdate)).EndInit();
            this._expandMenuConvertUnit.ResumeLayout(false);
            this._expandMenuConvertUnit.PerformLayout();
            this._expandMenuExtractDefine.ResumeLayout(false);
            this._expandMenuExtractDefine.PerformLayout();
            this._expandMenuCollectAbout.ResumeLayout(false);
            this._expandMenuCollectAbout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._fileSystemWatcherHotLoading)).EndInit();
            this._contextMenuEditOnHotLoading.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Sgry.Azuki.WinForms.AzukiControl _inputTextBox;
        private System.Windows.Forms.SplitContainer _splitContainer;
        private Sgry.Azuki.WinForms.AzukiControl _resultTextBox;
        private System.Windows.Forms.Timer _timerDelay;
        private Components.ImageButton _btnSave;
        private Components.ImageButton _btnLoad;
        private System.Windows.Forms.OpenFileDialog _openFileDialog;
        private System.Windows.Forms.SaveFileDialog _saveFileDialog;
        private Components.ImageButton _btnAbort;
        private System.Windows.Forms.PictureBox _picStatus;
        private System.Windows.Forms.ContextMenuStrip _contextMenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _menuUndo;
        private System.Windows.Forms.ToolStripMenuItem _menuRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _menuCut;
        private System.Windows.Forms.ToolStripMenuItem _menuCopy;
        private System.Windows.Forms.ToolStripMenuItem _menuPaste;
        private System.Windows.Forms.ToolStripMenuItem _menuDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem _menuSelectAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem _menuJumpDefine;
        private System.Windows.Forms.ToolStripMenuItem _menuAddUserDefine;
        private Components.ImageButton _btnSetting;
        private Components.ImageButton _btnAllDelete;
        private System.Windows.Forms.ContextMenuStrip _contextMenuResult;
        private System.Windows.Forms.ToolStripMenuItem _menuCopyResult;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem _menuSelectAllResult;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem _menuJumpDefineResult;
        private System.Windows.Forms.ToolStripMenuItem _menuAddUserDefineResult;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem _menuSolveSimultaneousEquation;
        private System.Windows.Forms.ToolStripMenuItem _menuCommentOut;
        private System.Windows.Forms.ToolStripMenuItem _menuUnCommentOut;
        private Components.ImageButton _btnMinimize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem _menuInsertHelp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem _menuSubstitute;
        private System.Windows.Forms.ToolStripMenuItem _menuDeformFormula;
        private System.Windows.Forms.ToolStripMenuItem _menuExpand;
        private System.Windows.Forms.ToolStripMenuItem _menuTidyUp;
        private System.Windows.Forms.ToolStripMenuItem _menuSimplify;
        private System.Windows.Forms.ToolStripMenuItem _menuFactorize;
        private System.Windows.Forms.ToolStripMenuItem _menuConvertUnit;
        private System.Windows.Forms.TreeView _treeViewHistory;
        private System.Windows.Forms.SplitContainer _splitContainerAll;
        private System.Windows.Forms.ToolStripMenuItem _menuVisibleDeformHistory;
        private System.Windows.Forms.ToolStripMenuItem _menuVisibleDeformHistoryResult;
        private System.Windows.Forms.ContextMenuStrip _contextMenuHistory;
        private System.Windows.Forms.ToolStripMenuItem _menuHideDeformHistory;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem _menuExpandHistory;
        private System.Windows.Forms.ToolStripMenuItem _menuFoldHistory;
        private System.Windows.Forms.ContextMenuStrip _contextMenuHistoryNode;
        private System.Windows.Forms.ToolStripMenuItem _menuCopyFormulaInHistory;
        private Components.ImageButton _btnHelp;
        private Components.ImageButton _btnUpdate;
        private System.Windows.Forms.Panel _panelFind;
        private System.Windows.Forms.TextBox _textBoxFind;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox _textBoxReplace;
        private Components.ImageButton _btnReplaceAndPrev;
        private Components.ImageButton _btnReplaceAndNext;
        private Components.ImageButton _btnFindPrev;
        private Components.ImageButton _btnFindNext;
        private System.Windows.Forms.Label _labelReplace;
        private System.Windows.Forms.Label _labelFind;
        private Components.ImageButton _btnHideFindPanel;
        private Components.ImageButton _btnToggleFindUseRegex;
        private Components.ImageButton _btnToggleFindMatchCase;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Components.ImageButton _btnReplaceAll;
        private System.Windows.Forms.ToolTip _toolTipFind;
        private Components.ImageButton _btnToggleFindPanelPosition;
        private System.Windows.Forms.ToolStripMenuItem _menuFindAndReplace;
        private System.Windows.Forms.ToolStripMenuItem _menuFindAndReplaceResult;
        private System.Windows.Forms.ToolStripMenuItem _menuDefineAsConstantOfFunction;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem _menuHideDeformHistoryNode;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem _menuExpandHistoryThisFormula;
        private System.Windows.Forms.ToolStripMenuItem _menuFoldHistoryThisFormula;
        private System.Windows.Forms.ToolStripMenuItem _menuExpandHistoryNode;
        private System.Windows.Forms.ToolStripMenuItem _menuFoldHistoryNode;
        private System.Windows.Forms.ContextMenuStrip _expandMenuConvertUnit;
        private System.Windows.Forms.ToolStripTextBox _txtBoxTargetUnit;
        private System.Windows.Forms.ToolTip _toolTipExpand;
        private System.Windows.Forms.ContextMenuStrip _expandMenuExtractDefine;
        private System.Windows.Forms.ToolStripTextBox _txtBoxDefineConstantOfFunctionName;
        private System.Windows.Forms.ContextMenuStrip _expandMenuSubstitute;
        private System.Windows.Forms.ToolStripMenuItem _menuCollectAbout;
        private System.Windows.Forms.ContextMenuStrip _expandMenuCollectAbout;
        private System.Windows.Forms.ToolStripTextBox _txtBoxCollectAbout;
        private System.Windows.Forms.ToolStripMenuItem _menuDeformAboutSelected;
        private System.Windows.Forms.Panel _panelHotLoading;
        private System.IO.FileSystemWatcher _fileSystemWatcherHotLoading;
        private Components.ImageButton _btnStopHotLoading;
        private System.Windows.Forms.TextBox _textBoxHotReloadingPath;
        private System.Windows.Forms.PictureBox _picHotReloading;
        private System.Windows.Forms.ToolStripMenuItem _menuStartHotLoading;
        private System.Windows.Forms.ContextMenuStrip _contextMenuEditOnHotLoading;
        private System.Windows.Forms.ToolStripMenuItem _menuCopyOnHotLoading;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem _menuSelectAllOnHotLoading;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem _menuFindOnHotLoading;
        private System.Windows.Forms.ToolStripMenuItem _menuVisibleDeformHistoryOnHotLoading;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem _menuStopHotLoading;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.Panel _panelHotSaving;
        private Components.ImageButton _btnStopHotSaving;
        private System.Windows.Forms.TextBox _textBoxHotSavingPath;
        private System.Windows.Forms.PictureBox _picHotSaving;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem _menuToggleHotSaving;
        private System.Windows.Forms.ToolStripMenuItem _menuInsertSigma;
        private System.Windows.Forms.ToolStripMenuItem _menuInsertPi;
        private Components.ImageButton _btnSaveSync;
    }
}
