﻿// -----------------------------------------------------------------------------
//  Copyright (C) 2016-2019 GoodSeat
//  Distributed under the MIT License
//  See https://sites.google.com/site/eatbaconandham/liffom/license 
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using GoodSeat.Liffom.Deforms.Rules;
using GoodSeat.Liffom.Formulas.Operators;

namespace GoodSeat.Liffom.Formulas.Rules
{
    /// <summary>
    /// 分子の数値の積算結果を規定する計算ルールを表します。
    /// a*b の数値化
    /// </summary>
    public class CalculateProductOfMolecularNumericRule : CombinationRule
    {
        static CalculateProductOfMolecularNumericRule s_entity;

        /// <summary>
        /// 分子の数値の積算結果を規定する計算ルールの実体を取得します。
        /// このルールにはプロパティが存在せず、再帰呼び出しにおいても結果は不変のため、通常この静的プロパティを使用することが推奨されます。
        /// </summary>
        public static CalculateProductOfMolecularNumericRule Entity
        {
            get
            {
                if (s_entity == null) s_entity = new CalculateProductOfMolecularNumericRule();
                return s_entity;
            }
        }

        protected override bool IsTargetCouple(Formula f1, Formula f2) { return f1 is Numeric && f2 is Numeric; }

        protected override Formula GetRuledFormula(Formula f1, Formula f2) { return new Numeric((f1 as Numeric).Figure * (f2 as Numeric).Figure); }

        protected internal override bool IsTargetTypeFormula(Formula target) { return target is Product; }

        public override string Information
        {
            get { return "分子の数値の積算を計算します。"; }
        }

        public override IEnumerable<KeyValuePair<Formula, Formula>> GetExamples()
        {
            yield return new KeyValuePair<Formula, Formula>(
                Formula.Parse("2*5*a/5"),
                Formula.Parse("10*a/5")
                );
        }

        protected override IEnumerable<Rule> OnGetAllPatternSample()
        {
            yield return Entity;
        }

        public override Rule GetClone()
        {
            return Entity;
        }

        /// <summary>
        /// 一つの組み合わせでルール適用があった場合に、再度全組み合わせについてルールの適用を試みる必要があるか否かを取得します。
        /// </summary>
        protected override bool RetryAll { get { return false; } }

        /// <summary>
        /// このルールの変形において、組み合わせが逆転しても結果が変わらないか否かを取得します。
        /// </summary>
        protected override bool Reversible { get { return true; } }
    }
}
