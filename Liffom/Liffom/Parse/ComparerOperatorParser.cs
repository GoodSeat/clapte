// -----------------------------------------------------------------------------
//  Copyright (C) 2016-2019 GoodSeat
//  Distributed under the MIT License
//  See https://sites.google.com/site/eatbaconandham/liffom/license 
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoodSeat.Liffom.Formulas;
using GoodSeat.Liffom.Formulas.Operators.Comparers;

namespace GoodSeat.Liffom.Parse
{
    /// <summary>
    /// 比較演算子の演算構文解析器を表します。
    /// </summary>
    public class ComparerOperatorParser : SeriesOperatorParser
    {
        /// <summary>
        /// 比較演算子の演算構文解析器を初期化します。
        /// </summary>
        public ComparerOperatorParser() { }

        public override IEnumerable<Lexer> GetAllBelongOperatorLexers()
        {
            yield return new ComparerOperatorLexer(this);
        }

        public override Formula GetParsedFormula(Formula initial, params KeyValuePair<OperatorToken, Formula>[] subsequents)
        {
            Formula result = initial;
            foreach (var pair in subsequents)
            {
                string mark = pair.Key.TargetText;
                var rhs = pair.Value;
                switch (mark)
                {
                    case "=":
                        result = new Equal(result, rhs);
                        break;
                    case "!=":
                        result = new NotEqual(result, rhs);
                        break;
                    case "<":
                        result = new LessThan(result, rhs, false);
                        break;
                    case "<=":
                        result = new LessThan(result, rhs, true);
                        break;
                    case ">":
                        result = new GreaterThan(result, rhs, false);
                        break;
                    case ">=":
                        result = new GreaterThan(result, rhs, true);
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            return result;
        }
    }
}
